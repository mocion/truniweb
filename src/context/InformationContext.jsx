import axios from "axios";
import React, { createContext, useEffect, useState } from "react";
import { getCollectionList } from "../helpers/firebase";
import { updateInfoActivity } from "../helpers/eviusActivities";
import { updateInfoEvent } from "../helpers/eviusEvents";

// organization: 616045724b799b2adb5aa523
// ??: 6160d3ba79e8a6299f700e7f
// truni: 60e4bd9c8e8ab015a0099463
// gob boyaca: 6169a0ad06812b515c004442
const organizationId = "616045724b799b2adb5aa523";
const eventId = "6160d3ba79e8a6299f700e7f";
// const token = `eyJhbGciOiJSUzI1NiIsImtpZCI6IjgwNTg1Zjk5MjExMmZmODgxMTEzOTlhMzY5NzU2MTc1YWExYjRjZjkiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vZXZpdXNhdXRoIiwiYXVkIjoiZXZpdXNhdXRoIiwiYXV0aF90aW1lIjoxNjM4Mjg5NDI0LCJ1c2VyX2lkIjoiYWJPQjlPNGwxbmVkZ0NGc3FTRXBlWnE1Z01rMSIsInN1YiI6ImFiT0I5TzRsMW5lZGdDRnNxU0VwZVpxNWdNazEiLCJpYXQiOjE2MzgyODk0MjQsImV4cCI6MTYzODI5MzAyNCwiZW1haWwiOiJtZXJjYWRlb0BjY2FjLmNvbS5jbyIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwiZmlyZWJhc2UiOnsiaWRlbnRpdGllcyI6eyJlbWFpbCI6WyJtZXJjYWRlb0BjY2FjLmNvbS5jbyJdfSwic2lnbl9pbl9wcm92aWRlciI6InBhc3N3b3JkIn19.YCvi2NZEDZINSXXq8f1IvKVWi64PJy6RhzrQCAKou0lrc5PgEl0LIv7XTXcQE4qgLx4EOVUSwz4_b75eGlgnRSGD2ZS1nwcuuY-rByl6gQSR_qIRqHoQ9MhXAOb0wG5SDIZFIiueLbVhLesQ1vjH5XW5QXYrD9IeF4kCyImorG8nGzqhxdc48Ora6ur61hcTBDCFYIWUIiKZLuGHtdOCQAgjsguq0XW4Z_WNGK_JcBeeHMGL1FB7AuI3OAe5iRZR7upQV_a83t0RR-ln1f24Xa-Yefb_mmZjJxLvBTomNvZ9hBF35YJVeDJE9tTqcYxj8qeCPBp87S7qxOja2BlQKg`;

export const InformationContext = createContext({});
export const InformationContextProvider = ({ children }) => {
  const [idEvent, setIdEvent] = useState(null);
  const [activities, setActivities] = useState(null);
  const [asignations, setAsignations] = useState([]);
  const [eventsList, setEventsList] = useState([{}]);
  const [monthEvents, setMonthEvents] = useState([{}]);
  const [type, setType] = useState([{}]);
  const [users, setUsers] = useState([]);
  const [selectedEvent, setSelectedEvent] = useState({});
  const [counterChanges, setCounterChanges] = useState(0);
  const [nameEvent, setNameEvent] = useState(null);
  const [userLogin, setUserLogin] = useState();
  const [token, setToken] = useState("hola");

  const path = `events/${idEvent ? idEvent : organizationId}/events`;

  useEffect(() => {
    const url = `https://api.evius.co/api/events/${eventId}/eventusers`;
    getCollectionList(path, setEventsList);
    getCollectionList(path, setMonthEvents);

    getCollectionList("asignations", setAsignations);
    console.log("debug asignations");
    getCollectionList("type", setType);
    axios(url).then((res) => {
      let lista = [];
      res.data.data.forEach((u) => {
        lista.push(u.properties);
      });

      lista = lista.filter((a) => a.id === "2117549036330");
      setUsers(lista);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (idEvent) {
      getCollectionList(path, setActivities);
    } else setActivities(null);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [idEvent]);

  useEffect(() => {
    if (selectedEvent.idEmployee && !selectedEvent.employee) {
      const find = users.find((a) => a.id === selectedEvent.idEmployee);
      const name = find && find.name;
      setSelectedEvent((event) => {
        const copy = { ...event, employee: name };
        return copy;
      });
    }
    if (selectedEvent.idAsignation && !selectedEvent.asignation) {
      const find = asignations.find((a) => a.id === selectedEvent.idAsignation);
      const name = find && find.name;
      setSelectedEvent((event) => {
        const copy = { ...event, asignation: name };
        return copy;
      });
    }
    if (selectedEvent.idAsignation || selectedEvent.idEmployee) {
      if (!selectedEvent.type) {
        setSelectedEvent((event) => {
          const copy = { ...event, type: "Working hours" };
          return copy;
        });
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedEvent]);

  useEffect(() => {
    var counter = 0;
    eventsList &&
      eventsList.forEach((event) => {
        event.notification && counter++;
      });
    setCounterChanges(counter);
  }, [eventsList]);

  if (!users || !asignations) {
    return null;
  }

  const value = {
    asignations,
    eventsList,
    type,
    users,
    selectedEvent,
    counterChanges,
    monthEvents,
    activities,
    organizationId,
    token,
    idEvent,
    path,
    nameEvent,
    userLogin,
    setUserLogin,
    setNameEvent,
    setIdEvent,
    setMonthEvents,
    setSelectedEvent,
    setToken,
    updateEvent: (event) => updateInfoEvent(event, token, organizationId),
    updateActivity: (event) => updateInfoActivity(event, idEvent),
  };

  return (
    <InformationContext.Provider value={value}>
      {children}
    </InformationContext.Provider>
  );
};
