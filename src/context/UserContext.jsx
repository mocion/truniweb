import React, { useState, useEffect } from "react";
import { auth } from "./../helpers/firebase";
import { onAuthStateChanged } from "firebase/auth";
import { signInAnonymously, signOut, updateProfile } from "firebase/auth";

export const UserContext = React.createContext();
export const UserProvider = UserContext.Provider;
export const UserConsumer = UserContext.Consumer;

export const UserContextProvider = ({ children }) => {
  const [user, setUser] = useState(undefined);

  /** API **/
  //El estado inicial del usuario hasta que se trae la info de la base de datos  es undefined
  const userIsLoading = user === undefined;

  let setUserInfo = async (usercred, data) => {
    //Usamos un hack para almacenar el rol porque updateProfile no permite agregar más campos
    await updateProfile(usercred.user, {
      displayName: JSON.stringify({ names: data.names, rol: data.rol }),
      photoURL:
        data.photoURL ||
        "https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png",
    });
    // await updateEmail(usercred.user, values.email);

    setUser((prevUser) => {
      return {
        ...prevUser,
        ...usercred,
        rol: data.rol,
        displayName: data.names,
        //email: values.email,
        photoURL:
          data.photoURL ||
          "https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png",
      };
    });
  };

  //Aqui se ejecuta el login en firebasse pero  onAuthStateChanged es quien avisa si ingresamos
  const logIn = async (values) => {
    const usercred = await signInAnonymously(auth);
    await setUserInfo(usercred, values);
  };
  //Aqui se ejecuta el logout en firebasse pero  onAuthStateChanged es quien avisa si nos deslogueamos
  const logOut = () => {
    signOut(auth);
  };

  /** Ejecutamos el listener de cambio de estado de la autenticación en firebase
   *  la sincronización VoxeetSDK la dejamos en otro useeffect que depende del estado User más limpio
   **/
  useEffect(function monitorAuthStateChangeFromFirebase() {
    onAuthStateChanged(auth, async (user) => {
      if (user) {
        let userFormated = parseStoredUser(user);
        setUser(userFormated);
      } else {
        setUser(null);
      }
    });
  }, []);

  return (
    <UserContext.Provider
      value={{
        user,
        setUserInfo,
        userIsLoading,
        logOut,
        logIn,
        setUser,
      }}
    >
      {children}
    </UserContext.Provider>
  );
};

/** HELPER FUNCTIONS */
const parseStoredUser = (user) => {
  let dataStoredHacked = { ...user };
  let hackedNamesandRol = JSON.parse(user.displayName);

  if (hackedNamesandRol && hackedNamesandRol.rol) {
    dataStoredHacked.rol = hackedNamesandRol.rol;
  }
  if (hackedNamesandRol && hackedNamesandRol.names) {
    dataStoredHacked.displayName = hackedNamesandRol.names;
  }
  return dataStoredHacked;
};

export default UserContext;
