import React, { useState, createContext, useContext, useEffect } from "react";
import { ViewContext } from "./ViewContext";
import { InformationContext } from "./InformationContext";
import { newArrayNames } from "../helpers/configurations";

export const CalendarContext = createContext({});
export const CalendarContextProvider = ({ children }) => {
  let { viewUser, screen } = useContext(ViewContext);
  let {
    activities,
    users,
    asignations,
    eventsList,
    monthEvents,
    setMonthEvents,
  } = useContext(InformationContext);

  const [view, setView] = useState("month");
  const [date, setDate] = useState(new Date());
  const [filter, setFilter] = useState([]);

  useEffect(() => {
    setFilter(newArrayNames(users));
  }, [users]);

  useEffect(() => {
    const copy = [];
    eventsList.forEach((event) => {
      const value = viewUser ? event.employee : event.asignation;
      filter.includes(value) && copy.push(event);
    });
    setMonthEvents(copy);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filter]);

  useEffect(() => {
    if (viewUser) {
      const array = newArrayNames(users);
      setFilter(array);
    } else {
      const array = newArrayNames(asignations);
      setFilter(array);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [screen]);

  //<--------------------------------------------------->
  const accesor = viewUser ? "idEmployee" : "idAsignation";
  const dayOrWeek = view === "day" || view === "week";
  const table = viewUser ? users : asignations;
  const options = [];
  table.forEach((item) => item?.name && options.push(item.name));
  const events = activities
    ? activities
    : dayOrWeek
    ? eventsList
    : monthEvents;

  const value = {
    date,
    view,
    accesor,
    events,
    filter,
    options,
    setFilter,
    setDate,
    setView,
  };

  return (
    <CalendarContext.Provider value={value}>
      {children}
    </CalendarContext.Provider>
  );
};
