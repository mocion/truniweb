import React, { createContext, useState } from "react";

export const ViewContext = createContext({});
export const ViewContextProvider = ({ children }) => {
  const [modalVisible, setModalVisible] = useState();
  const [screen, setScreen] = useState("user");

  const value = {
    screen,
    modalCreate: modalVisible === "create",
    modalDetail: modalVisible === "detail",
    modalSimple: modalVisible === "simple",
    modalDelete: modalVisible === "delete",
    modalHours: modalVisible === "hours",
    modalFilter: modalVisible === "filter",
    modalClone: modalVisible === "clone",
    modalUpload: modalVisible === "upload",
    viewCms: screen === "cms",
    viewUser: screen === "user",
    viewSpace: screen === "space",
    onCancel: () => setModalVisible(null),
    handleModalCreate: () => setModalVisible("create"),
    handleModalDetail: () => setModalVisible("detail"),
    handleModalSimple: () => setModalVisible("simple"),
    handleModalDelete: () => setModalVisible("delete"),
    handleModalHours: () => setModalVisible("hours"),
    handleModalFilter: () => setModalVisible("filter"),
    handleModalClone: () => setModalVisible("clone"),
    handleModalUpload: () => setModalVisible("upload"),
    handleViewCms: () => setScreen("cms"),
    handleViewUser: () => setScreen("user"),
    handleViewSpace: () => setScreen("space"),
  };

  return <ViewContext.Provider value={value}>{children}</ViewContext.Provider>;
};
