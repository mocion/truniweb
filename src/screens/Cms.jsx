import { Card, Space } from "antd";
import React, { useContext } from "react";
import { InformationContext } from "../context/InformationContext";
import { setNewDoc } from "../helpers/firebase";
import Notifications from "../components/Notifications";
import { CmsItem } from "../components/CmsItem";

export const Cms = () => {
  let { asignations, type } = useContext(InformationContext);
  return (
    <Space style={style}>
      <Card>
        <CmsItem title="asignations" setter={setNewDoc} list={asignations} />
      </Card>
      <Card>
        <CmsItem title="type" setter={setNewDoc} list={type} />
      </Card>
      <Card>
        <Notifications />
      </Card>
    </Space>
  );
};

const style = {
  display: "grid",
  gap: "1rem",
  gridAutoRow: "22rem",
  gridTemplateColumns: "repeat(auto-fill, minmax(min(100%, 25rem), 1fr))",
};
