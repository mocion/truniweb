import React, { useContext } from "react";
import { setNewDoc } from "../helpers/firebase";
import { CalendarComponent } from "../components/Calendar.component";
import { CalendarContext } from "../context/CalendarContext";
import { ViewContext } from "../context/ViewContext";
import { InformationContext } from "../context/InformationContext";
import UserContext from "../context/UserContext";

const now = () => new Date();
let selectTimeout;

export const Calendar = () => {
  const { viewUser, handleModalCreate, handleModalDetail, handleModalSimple } =
    useContext(ViewContext);
  const {
    path,
    updateActivity,
    updateEvent,
    users,
    asignations,
    setSelectedEvent,
  } = useContext(InformationContext);
  const { date, setDate, view, setView, events, accesor, filter } =
    useContext(CalendarContext);
  const { user } = useContext(UserContext);

  const onSelectSlot = ({ start, end, action, resourceId }) => {
    selectTimeout && window.clearTimeout(selectTimeout);

    selectTimeout = setTimeout(() => {
      setSelectedEvent((event) => {
        var newEvent = { ...event };
        const random = Math.random() * Math.floor(Math.random() * Date.now());
        newEvent.id = Date.now() + Math.floor(random);
        viewUser
          ? (newEvent.idEmployee = resourceId)
          : (newEvent.idAsignation = resourceId);
        newEvent.start = start;
        newEvent.end = end;
        return newEvent;
      });
      user && handleModalCreate();
    }, 250);
  };

  const onSelectEvent = (event) => {
    selectTimeout && window.clearTimeout(selectTimeout);

    setSelectedEvent(event);
    selectTimeout = setTimeout(() => {
      user ? handleModalDetail() : handleModalSimple();
    }, 250);
  };

  const onDoubleClickEvent = (event) => {
    if (user) {
      selectTimeout && window.clearTimeout(selectTimeout);

      selectTimeout = setTimeout(() => {
        console.log("onDoubleClickEvent: ", event);
      }, 250);
    }
  };

  const moveEvent = ({ event, start, end }) => {
    if (user) {
      const updatedEvent = { ...event, start, end, notification: true };
      event.idEvent && updateEvent(updatedEvent);
      event.idActivity && updateActivity(updatedEvent);
      setNewDoc(path, updatedEvent);
    }
  };

  const resizeEvent = ({ event, start, end }) => {
    if (user) {
      const updatedEvent = { ...event, start, end, notification: true };
      event.idEvent && updateEvent(updatedEvent);
      event.idActivity && updateActivity(updatedEvent);
      setNewDoc(path, updatedEvent);
    }
  };

  const onKeyPressEvent = ({ event, ...other }) => {
    console.log("[onKeyPressEvent] - event", event);
    console.log("[onKeyPressEvent] - other", other);
  };

  const onSelecting = (range) => {
    // console.log("[onSelecting] range: ", range);
  };

  function eventStyleGetter(event, start, end) {
    if (!event || !event.idAsignation || !event.idEmployee) {
      console.log("ups", event);
      return {
        style: {},
      };
    }
    const asignation =
      asignations && asignations.find((a) => a.id === event.idAsignation);
    const employee = users && users.find((a) => a.id === event.idEmployee);

    var style = {
      backgroundColor:
        event.type === "Break" || event.type === "Not available to work"
          ? "#bfbfbf"
          : viewUser
          ? asignation
            ? asignation.hexColor
            : "#fff"
          : employee
          ? employee.hexColor
          : "#fff",
    };
    return {
      style: style,
    };
  }

  const resourceFiltered = () => {
    const result = [];
    const resource = viewUser ? users : asignations;
    resource.forEach((item) => {
      filter.includes(item.name) && result.push(item);
    });
    return result;
  };

  const props = {
    events,
    date,
    view,
    style: { height: "90%" },
    onSelectSlot,
    onSelectEvent,
    onSelecting,
    onDoubleClickEvent,
    onKeyPressEvent,
    onView: (newView) => setView(newView),
    onNavigate: (newDate) => setDate(newDate),
    draggableAccessor: (event) => !event.blocked,
    resizableAccessor: (event) => !event.blocked,
  };

  return (
    <CalendarComponent
      {...props}
      onEventDrop={moveEvent}
      onEventResize={resizeEvent}
      getNow={now}
      selectable="ignoreEvents"
      resources={resourceFiltered()}
      titleAccessor={(e) => {
        let title = "";
        if (viewUser) {
          let asignation = asignations.find((a) => a.id === e.idAsignation);
          title = (asignation && asignation.name) || "-";
        } else {
          let employee = users.find((a) => a.id === e.idEmployee);
          title = (employee && employee.names) || "-";
        }
        let htmltitle =
          (e.name ? e.name : e.description ? e.description : "") + "  " + title;
        title = (
          <span
            dangerouslySetInnerHTML={{
              __html: (e.idEvent ? "&#128198;" : "") + htmltitle,
            }}
          />
        );
        return title;
      }}
      resourceAccessor={accesor}
      resourceIdAccessor="id"
      resourceTitleAccessor="name"
      step={30}
      defaultView="month"
      eventPropGetter={eventStyleGetter}
    />
  );
};
