import firebase from "firebase/compat/app";
import { auth } from "./../helpers/firebase";
import { Layout, Breadcrumb } from "antd";
import StyledFirebaseAuth from "react-firebaseui/StyledFirebaseAuth";

import React from "react";
import { useNavigate } from "react-router-dom";

const { Header, Content, Footer } = Layout;
// Initialize the FirebaseUI Widget using Firebase.

firebase.auth().onAuthStateChanged((user) => {
  if (user) {
    // User is signed in, see docs for a list of available properties
    // https://firebase.google.com/docs/reference/js/firebase.User
    // var uid = user.uid;
    // ...
  } else {
    // User is signed out
    // ...
  }
});

export const Login = () => {
  const navigate = useNavigate();

  const uiConfig = {
    signInOptions: [
      {
        provider: firebase.auth.EmailAuthProvider.PROVIDER_ID,
        disableSignUp: {
          status: true,
        },
        signInSuccess: (currentUser, credential, redirectUrl) => {
          navigate("/");
          return false;
        },
        signInSuccessWithAuthResult: (authResult, redirectUrl) => {
          navigate("/");
          return false;
        },
      },
    ],
    // the area where i assume something is going wrong...
    callbacks: {
      signInSuccess: (currentUser, credential, redirectUrl) => {
        navigate("/");
        return false;
      },
      signInSuccessWithAuthResult: (authResult, redirectUrl) => {
        navigate("/");
        return false;
      },
    },
  };

  return (
    <Layout className="layout">
      <Header>
        <div className="logo" />
      </Header>
      <Content style={{ padding: "0 50px" }}>
        <Breadcrumb style={{ margin: "16px 0" }}></Breadcrumb>
        <div className="site-layout-content">
          <StyledFirebaseAuth uiConfig={uiConfig} firebaseAuth={auth} />
        </div>
      </Content>
      <Footer style={{ textAlign: "center" }}>TRUNI EVIUS</Footer>
    </Layout>
  );
};
