import axios from "axios";
import moment from "moment";

import {auth} from './../helpers/firebase'

export const updateInfoEvent = (event, token, idOrganization) => {
  const url = `https://api.evius.co/api/events/${event.idEvent}?token=${token}`;
  const start = moment(event.start).format("YYYY-MM-DD HH:mm:ss");
  const end = moment(event.end).format("YYYY-MM-DD HH:mm:ss");
  const data = {
    name: event.name || "",
    description: event.description || "",
    datetime_from: start,
    datetime_to: end,
    organizer_id: idOrganization,
    author_id: idOrganization,
    event_type_id: "5bf47203754e2317e4300b68",
  };
  axios.put(url, data);
};



export const createEvent = async (event, token, organizationId) => {

  token = await auth.currentUser.getIdToken();
  console.log(token);
  const url = `https://api.evius.co/api/events?token=${token}`;
  const start = moment(event.start).format("YYYY-MM-DD HH:mm:ss");
  const end = moment(event.end).format("YYYY-MM-DD HH:mm:ss");
  const data = {
    name: event.name || "",
    address: "",
    type_event: "onlineEvent",
    datetime_from: start,
    datetime_to: end,
    picture: null,
    venue: "",
    location: "",
    visibility: "PUBLIC",
    description: "",
    category_ids: [],
    organizer_id: organizationId,
    event_type_id: "5bf47203754e2317e4300b68",
    user_properties: [],
    allow_register: true,
    styles: {
      buttonColor: "#FFF",
      banner_color: "#FFF",
      menu_color: "#FFF",
      event_image: null,
      banner_image: null,
      menu_image: null,
      brandPrimary: "#FFFFFF",
      brandSuccess: "#FFFFFF",
      brandInfo: "#FFFFFF",
      brandDanger: "#FFFFFF",
      containerBgColor: "#ffffff",
      brandWarning: "#FFFFFF",
      toolbarDefaultBg: "#FFFFFF",
      brandDark: "#FFFFFF",
      brandLight: "#FFFFFF",
      textMenu: "#555352",
      activeText: "#FFFFFF",
      bgButtonsEvent: "#FFFFFF",
      banner_image_email: null,
      BackgroundImage: null,
      FooterImage: null,
      banner_footer: null,
      mobile_banner: null,
      banner_footer_email: null,
      show_banner: "true",
      show_card_banner: false,
      show_inscription: false,
      hideDatesAgenda: true,
      hideDatesAgendaItem: false,
      hideHoursAgenda: false,
      hideBtnDetailAgenda: true,
      loader_page: "no",
      data_loader_page: null,
    },
  };
  const result = await axios.post(url, data);
  return result.data._id;
};
