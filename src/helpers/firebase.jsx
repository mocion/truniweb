import firebase from "firebase/compat/app";
import "firebase/compat/auth";
import { getAuth } from "firebase/auth";
import "firebase/compat/firestore";
import moment from "moment";
import axios from "axios";

var firebaseConfig = {
  apiKey: "AIzaSyAGhnaznb-fY_ysdz2UhoLouvIOYBqO4vg",
  authDomain: "truni-21c5b.firebaseapp.com",
  projectId: "truni-21c5b",
  storageBucket: "truni-21c5b.appspot.com",
  messagingSenderId: "756379773468",
  appId: "1:756379773468:web:353232b3a7a504d89bcdd9",
  measurementId: "G-14N7XY7K2Z",
};

var firebaseConfigEvius = {
  apiKey: "AIzaSyDDnc9WHXf4CWwXCVggeiarYGu_xBgibJY",
  authDomain: "eviusauth.firebaseapp.com",
  databaseURL: "https://eviusauth.firebaseio.com",
  projectId: "eviusauth",
  storageBucket: "eviusauth.appspot.com",
  messagingSenderId: "400499146867",
  appId: "1:400499146867:web:5d0021573a43a1df",
};

const fbEvius = firebase.initializeApp(firebaseConfigEvius, "evius");
//export const auth = fbEvius.auth();
export const auth = getAuth(fbEvius);


const firebaseFirestore = firebase.initializeApp(firebaseConfig);
export const db = firebaseFirestore.firestore();

export const signInUser = (email, pass, setter, setToken) => {
  return auth.signInWithEmailAndPassword(email, pass)
  // .then((userCredential) => {
  //   userCredential.user.getIdToken(true).then(function (idToken) {
  //     setToken(idToken);
  //   });
  //   setter(true);
  // });
};

export const logoutUser = (setter) => {
  auth.signOut().then(() => setter(null));
};

export function getCollectionList(collection, setValues) {
  db.collection(collection).onSnapshot((querySnapshot) => {
    var lista = [];
    querySnapshot.forEach((doc) => {
      var item = doc.data();

      if (collection.split("/")[0] === "events" && item.start) {
        item.start = item.start.toDate();
        item.end = item.end.toDate();
        lista.push(item);
      } else {
        lista.push(item);
      }
    });
    setValues(() => {
      return lista;
    });
  });
}

export function sendToFilteredTokens(users, field, filter, title, msg, bool) {
  users.forEach((user) => {
    if (user.token) {
      const body = {
        title: title,
        message: msg,
        token: user.token,
      };
      if (bool && user[field] === filter) {
        sendNotification(body);
      } else if (!bool) {
        sendNotification(body);
      }
    }
  });
}

export function setNewDoc(collection, value) {
  if (value) {
    db.collection(collection).doc(`${value.id}`).set(value);
  }
}

export function deleteEvent(value, path) {
  if (value) {
    db.collection(path).doc(`${value.id}`).delete();
  }
}

export function doubleEvents(minStart, targetDate, path) {
  var maxStart = new Date(minStart);
  maxStart = maxStart.setDate(maxStart.getDate() + 1);
  maxStart = new Date(maxStart);

  db.collection(path)
    .where("start", ">=", minStart)
    .where("start", "<=", maxStart)
    .get()
    .then((documents) => {
      documents.forEach((doc) => {
        var item = doc.data();
        var random = Math.random() * Math.floor(Math.random() * Date.now());
        var hourStart = moment(item.start.toDate()).format("HH:mm");
        const resta = (item.end - item.start) * 1000;
        item.start = new Date(`${targetDate} ${hourStart}`);
        item.id = Date.now() + Math.floor(random);
        item.end = new Date(item.start.getTime() + resta);
        setNewDoc(path, item);
      });
    });
}

export async function notificationEvents(users, path) {
  var counterChanges = 0;

  const events = await db.collection(path).where("notification", "==", true).get();

  const infoArray = infoEventsArray(events);
  events.forEach((event) => {
    counterChanges += 1;
    const data = event.data();
    data.notification = false;
    db.collection(path).doc(`${data.id}`).set(data);
  });

  Object.keys(infoArray).foreach(async (email) => {
    const token = users.find((a) => a.email === email).token;
    const calc = Math.random() * Math.floor(Math.random() * Date.now());
    const id = Date.now() + Math.floor(calc);
    const date = Date.now();
    const value = infoArray[email];
    const data = { id, email, value, date };
    await db.collection("notifications").doc(`${id}`).set(data);

    if (token) {
      const body = {
        title: "There was changes:\n",
        message: value,
        token: token,
      };
      await sendNotification(body);
    }
  });
  return counterChanges;
}

export const sendNotification = async (body) => {
  await axios.post("https://eviusauth.web.app/notification", body);
};

const infoEventsArray = (events) => {
  var array = {};
  events.forEach((event) => {
    const e = event.data();
    const start = moment(e.start.toDate()).format("MM/DD HH:mm");
    const end = moment(e.end.toDate()).format("HH:mm");
    const msg = `${e.asignation}: ${start}-${end}\n`;
    if (e.email in array) {
      array[e.email] += msg;
    } else {
      array[e.email] = msg;
    }
  });
  return array;
};
