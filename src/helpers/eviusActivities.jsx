import axios from "axios";
import moment from "moment";
export const updateInfoActivity = (event, idEvent) => {
  const url = `https://api.evius.co/api/events/${idEvent}/activities/${event.idActivity}`;
  const start = moment(event.start).format("YYYY-MM-DD HH:mm");
  const end = moment(event.end).format("YYYY-MM-DD HH:mm");
  // pendiente la creación de eventos nuevos
  const data = {
    name: event.name, 
    description: event.description || "",
    capacity: 50,
    event_id: idEvent,
    datetime_end: end,
    datetime_start: start,
  };
  idEvent && axios.put(url, data);
};

export const createActivity = async (event, idEvent) => {
  const url = `https://api.evius.co/api/events/${idEvent}/activities`;
  const start = moment(event.start).format("YYYY-MM-DD HH:mm");
  const end = moment(event.end).format("YYYY-MM-DD HH:mm");
  const data = {
    name: event.name || "",
    subtitle: event.subtitle || "",
    description: event.description || "",
    capacity: 50,
    event_id: idEvent,
    datetime_end: end,
    datetime_start: start,
  };
  const result = await axios.post(url, data);
  return result.data._id;
};
