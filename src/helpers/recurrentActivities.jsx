import moment from "moment";
import { RRule } from "rrule";

// https://www.npmjs.com/package/rrule
// Create a rule:
export function RecurrentActivities({ date, checkedValues, selectedEvent }) {
  const array = [];
  const eventStart = moment(selectedEvent.start);
  const eventEnd = moment(selectedEvent.end);
  var hourStart = eventStart.hour();
  var hourEnd = eventEnd.hour();
  if (hourStart === hourEnd) {
    hourStart < 23 ? (hourEnd += 1) : (hourStart -= 1);
  }
  const size = hourStart < hourEnd;
  var starts = `${size ? hourStart : hourEnd}:${eventStart.minute()}:00`;
  var ends = `${size ? hourEnd : hourStart}:${eventEnd.minute()}:00`;

  const rule = new RRule({
    freq: RRule.WEEKLY,
    byweekday: checkedValues,
    dtstart: date[0]._d,
    until: date[1]._d,
  });

  rule.all().forEach((item) => {
    var time = moment(item);
    var itemDate = `${time.month() + 1}/${time.date()}/${time.year()}`;
    var dateStart = moment(`${itemDate} ${starts}`)._d;
    var dateEnd = moment(`${itemDate} ${ends}`)._d;
    const object = { start: dateStart, end: dateEnd };
    array.push(object);
  });

  return array;
}
