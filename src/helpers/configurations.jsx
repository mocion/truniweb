export const EVIUS_URL  = "https://staging.evius.co"

export const newArrayNames = (array) => {
  const newArray = [];
  array.forEach((item) => {
    const name = item.name || "";
    newArray.push(name);
  });
  return newArray;
};

export const createListEvents = (list) => {
  const arrayItems = [];
  list.forEach((event) => {
    const start = new Date(event.datetime_start);
    const end = new Date(event.datetime_end);
    const random = Math.random() * Math.floor(Math.random() * Date.now());
    const id = Date.now() + Math.floor(random);
    const data = {
      notification: false,
      end,
      idEmployee: 1634872880034,
      type: "Working hours",
      id,
      description: event.name,
      employee: "Negocios",
      email: "test.mail@mocion.com",
      asignation: "Aliados",
      idAsignation: 1675425946071,
      idActivity: event._id,
      start,
    };
    arrayItems.push(data);
  });
  return arrayItems;
};
