import React, { useContext } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { HeaderContent } from "../components/HeaderContent";
import { Cms } from "../screens/Cms";
import { Login } from "../screens/Login";
import { Calendar } from "../screens/Calendar";
import FilterHandler from "../components/modals/FilterHandler.jsx";
import CopyHandler from "../components/modals/CopyHandler.jsx";
import HoursHandler from "../components/modals/HoursHandler.jsx";
import DetailActivityModal from "../components/modals/detailActivityModal.jsx";
import DeleteActivityModal from "../components/modals/deleteActivityModal.jsx";
import CreateActivityModal from "../components/modals/createActivityModal.jsx";
import { UserContext } from "../context/UserContext";
import { Example } from "../helpers/printComponent";
import { UploadDocument } from "../components/modals/UploadDocument";
import SimpleDetailModal from "../components/modals/SimpleDetailModal";
import { Layout } from "antd";
const { Header, Content } = Layout;

export function MainRoutes() {
  const { userIsLoading } = useContext(UserContext);

  if (userIsLoading) return <h1>Loading....</h1>;
  return (
    <BrowserRouter>
      <Layout theme="light">
        <Header>
        <HeaderContent />
        </Header>
        <Content>
          <Routes>
            <Route exact path="/calendar" element={<Calendar />} />
            <Route exact path="/cms" element={<Cms />} />
            <Route exact path="/example" element={<Example />} />
            <Route exact path="/login" element={<Login />} />
            <Route exact path="/" element={<Calendar />} />
          </Routes>
        </Content>
        {/* <Footer>Footer</Footer> */}
      </Layout>
      <DetailActivityModal />
      <DeleteActivityModal />
      <CreateActivityModal />
      <SimpleDetailModal />
      <HoursHandler />
      <FilterHandler />
      <CopyHandler />
      <UploadDocument />
    </BrowserRouter>
  );
}
