import React, { useState, useContext } from "react";
import { Modal, Button, message, Space, Radio } from "antd";
import { setNewDoc } from "../../helpers/firebase";
import InfoSelectors from "../InfoSelectors.jsx";
import { InformationContext } from "../../context/InformationContext";
import { createActivity } from "../../helpers/eviusActivities";
import { createEvent } from "../../helpers/eviusEvents";
import { ViewContext } from "../../context/ViewContext";

export default function CreateActivityModal() {
  let { onCancel, modalCreate } = useContext(ViewContext);
  let {
    selectedEvent,
    setSelectedEvent,
    path,
    idEvent,
    token,
    organizationId,
  } = useContext(InformationContext);

  const [radioValue, setRadioValue] = useState(true);

  const cleanSettings = () => {
    setSelectedEvent({});
    setRadioValue(true);
    onCancel();
  };

  const handleCreate = async () => {
    //Variables por defecto para evitar muchos problemas que se han presentado
    selectedEvent.idAsignation = selectedEvent.idAsignation
      ? selectedEvent.idAsignation
      : 1635791371036;
    selectedEvent.idEmployee = selectedEvent.idEmployee
      ? selectedEvent.idEmployee
      : 2117549036330;

    if (
      selectedEvent.employee &&
      selectedEvent.asignation &&
      selectedEvent.name
    ) {
      let result = null;
      if (radioValue) {
        if (idEvent) {
          result = await createActivityEvius();
        } else {
          result = await createEventEvius();
        }
      }
      createShift(result);
    } else {
      message.error("Please fill all the information");
    }
  };

  const createShift = async (id) => {
    selectedEvent.notification = true;
    if (id) {
      idEvent ? (selectedEvent.idActivity = id) : (selectedEvent.idEvent = id);
    }
    await setNewDoc(path, selectedEvent);
    message.success("Event created");
    cleanSettings();
  };

  const createEventEvius = async () => {
    const result = await createEvent(selectedEvent, token, organizationId);
    return result;
  };

  const createActivityEvius = async () => {
    const result = await createActivity(selectedEvent, idEvent);
    return result;
  };

  const onChange = (e) => setRadioValue(e.target.value);

  const value1 = "Create an event with internal schedule";
  const value2 = "Create a new schedule activity";

  return (
    <Modal
      title={`Creation`}
      visible={modalCreate}
      onCancel={cleanSettings}
      footer={[
        <Button key="Crear" onClick={handleCreate}>
          Create
        </Button>,
      ]}
    >
      <Radio.Group onChange={onChange} value={radioValue}>
        <Space direction="vertical">
          <Radio value={true}>{idEvent ? value2 : value1}</Radio>
          {!idEvent && <Radio value={false}>{value2}</Radio>}
        </Space>
      </Radio.Group>
      <br />
      <InfoSelectors /> <br /> <br />
    </Modal>
  );
}
