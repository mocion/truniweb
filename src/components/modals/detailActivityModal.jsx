import React, { useContext, useEffect, useState } from "react";
import { Modal, Button, message, Space, Checkbox } from "antd";
import { deleteEvent, setNewDoc } from "../../helpers/firebase.jsx";
import InfoSelectors from "../InfoSelectors.jsx";
import axios from "axios";
import { InformationContext } from "../../context/InformationContext";
import { ViewContext } from "../../context/ViewContext";
import UserContext from "../../context/UserContext.jsx";

export default function DetailActivityModal() {
  const {
    selectedEvent,
    setSelectedEvent,
    path,
    setIdEvent,
    idEvent,
    updateActivity,
    updateEvent,
    setNameEvent,
    organizationId,
  } = useContext(InformationContext);
  const { onCancel, handleModalDelete, modalDetail } = useContext(ViewContext);
  const { user } = useContext(UserContext);

  const [listActivities, setListActivities] = useState(null);
  const [listUsers, setListUsers] = useState(null);
  const [listCounter, setListCounter] = useState(0);
  const [sendNotification, setSendNotification] = useState(false);

  const activities = listActivities && listActivities.length > 0;
  const url_rsvp = `https://apidev.evius.co/api/rsvp/sendeventrsvp/${selectedEvent.idEvent}`;

  useEffect(() => {
    if (listActivities) {
      var counter = 0;
      listActivities.forEach((item) => {
        counter++;
      });
      setListCounter(counter);
    }
  }, [listActivities]);

  useEffect(() => {
    if (selectedEvent.idEvent) {
      const url_eventUsers = `https://api.evius.co/api/events/${selectedEvent.idEvent}/eventUsers`;
      const url_activities = `https://api.evius.co/api/events/${selectedEvent.idEvent}/activities`;
      let users = [];
      axios(url_activities).then((r) => setListActivities(r.data.data));
      axios(url_eventUsers)
        .then((r) => {
          r.data.data.forEach((user) => users.push(user._id));
        })
        .then(() => setListUsers(users));
    }
  }, [selectedEvent]);

  const handleChanges = () => {
    if (sendNotification) {
      const data = {
        subject: "Titulo del correo",
        message: "Este es el mensaje de confirmación del correo",
        eventUsersIds: listUsers,
      };
      axios.post(url_rsvp, data);
      setSendNotification(false);
    }

    delete selectedEvent["sourceResource"];
    selectedEvent.idEvent && updateEvent(selectedEvent);
    selectedEvent.idActivity && updateActivity(selectedEvent);
    setNewDoc(path, selectedEvent);
    message.info("changes made successfully");
    onCancel();
  };

  const handleCancel = () => {
    setListActivities(null);
    setSelectedEvent({});
    onCancel();
  };

  const showActivities = () => {
    setNameEvent(selectedEvent.description);
    setIdEvent(selectedEvent.idEvent);
    setSelectedEvent({});
    onCancel();
  };

  const moveToPrincipal = () => {
    const pathOrg = `events/${organizationId}/events`;
    setNewDoc(pathOrg, selectedEvent);
    deleteEvent(selectedEvent, path);
    setSelectedEvent({});
    onCancel();
  };

  const url = `https://app.evius.co/eventadmin/${selectedEvent.idEvent}/agenda`;

  const onChange = (e) => setSendNotification(e.target.checked);

  return (
    <Modal
      title="Information"
      visible={modalDetail}
      onCancel={handleCancel}
      footer={[
        <Button disabled={!user} key="Delete" onClick={handleModalDelete}>
          Delete
        </Button>,
        <Button
          disabled={!user}
          type="primary"
          key="Save"
          onClick={handleChanges}
        >
          Save changes
        </Button>,
      ]}
    >
      <InfoSelectors
        key={selectedEvent.id}
        activities={activities}
        listCounter={listCounter}
        showActivities={showActivities}
        url={url}
      />
      {selectedEvent.idEvent && (
        <Space direction="vertical">
          <Checkbox checked={sendNotification} onChange={onChange}>
            Send users schedule confirmation
          </Checkbox>
        </Space>
      )}
      {idEvent && (
        <Button onClick={moveToPrincipal}>Move to principal calendar</Button>
      )}
    </Modal>
  );
}
