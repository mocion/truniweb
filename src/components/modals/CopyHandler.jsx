import React, { useContext, useState } from "react";
import { Button, DatePicker, message, Modal } from "antd";
import { ViewContext } from "../../context/ViewContext";
import moment from "moment";
import { doubleEvents } from "../../helpers/firebase";
import { CalendarContext } from "../../context/CalendarContext";
import { InformationContext } from "../../context/InformationContext";

const CopyHandler = () => {
  const { path } = useContext(InformationContext);
  const { onCancel, modalClone: visible } = useContext(ViewContext);
  const { date } = useContext(CalendarContext);

  const [copyDate, setCopyDate] = useState(false);

  const onChange = (date, dateString) => setCopyDate(dateString);

  const cloneEvents = () => {
    if (copyDate) {
      const newDate = moment(date).format("YYYY-MM-DD") + " 00:00";
      doubleEvents(new Date(newDate), copyDate, path);
      onCancel();
      message.info("events duplicated");
    } else {
      message.error("You must select a date to do a copy");
    }
  };

  const props = { visible, onCancel, title: "Duplicate shifts" };
  return (
    <Modal {...props} footer={[<Button onClick={cloneEvents}>Check</Button>]}>
      <p>When do you want to make this copy?</p>
      <DatePicker onChange={onChange} />
    </Modal>
  );
};

export default CopyHandler;
