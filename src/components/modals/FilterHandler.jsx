import React, { useContext } from "react";
import { Checkbox, Modal } from "antd";
import { ViewContext } from "../../context/ViewContext";
import { CalendarContext } from "../../context/CalendarContext";

const FilterHandler = () => {
  const { options, setFilter } = useContext(CalendarContext);
  const { onCancel, modalFilter: visible } = useContext(ViewContext);

  const onChange = (values) => {
    if (values.length > 0) {
      setFilter(values);
    } else setFilter(options);
  };

  return (
    <Modal {...{ onCancel, visible, title: "Filter values" }} footer={[]}>
      <Checkbox.Group {...{ options, onChange }} />
    </Modal>
  );
};

export default FilterHandler;
