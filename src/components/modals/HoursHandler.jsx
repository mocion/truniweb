import React, { useContext } from "react";
import "antd/dist/antd.css";
import { Modal, Table } from "antd";
import { ViewContext } from "../../context/ViewContext";
import { InformationContext } from "../../context/InformationContext";

const HoursHandler = () => {
  const { eventsList } = useContext(InformationContext);
  const { onCancel, modalHours } = useContext(ViewContext);

  const getDataTable = (array) => {
    const result = [];
    const names = {};
    var counter = 0;
    // array.forEach((item) => {
      // const date = `${item.start.getMonth() + 1}-${item.start.getDate()}`;
      // const hours = item.end.getHours() - item.start.getHours();
      // !names[item.employee] && (names[item.employee] = {});
      // if (names[item.employee][date]) {
      //   names[item.employee][date] = names[item.employee][date] + hours;
      // } else {
      //   names[item.employee][date] = hours;
      // }
    // });
    Object.keys(names).forEach((name) => {
      Object.keys(names[name]).forEach((date) => {
        counter++;
        result.push({
          key: counter,
          name: name,
          date: date,
          hours: names[name][date],
        });
      });
    });
    return result;
  };

  const getColumnsTable = (array) => {
    const result = [
      {
        title: "Name",
        dataIndex: "name",
        filters: [],
        onFilter: (value, record) => record.names.indexOf(value) === 0,
        sorter: (a, b) => a.names.length - b.names.length,
        sortDirections: ["descend"],
      },
      {
        title: "Date",
        dataIndex: "date",
        defaultSortOrder: "descend",
        filters: [],
        onFilter: (value, record) => record.date.indexOf(value) === 0,
      },
      {
        title: "Hours",
        dataIndex: "hours",
        defaultSortOrder: "descend",
        sorter: (a, b) => a.hours - b.hours,
      },
    ];
    const names = {};
    const dates = {};
    // array.forEach((item) => {
    //   const date = `${item.start.getMonth() + 1}-${item.start.getDate()}`;
    //   dates[date] = date;
    //   names[item.employee] = item.employee;
    // });
    Object.values(names).forEach((name) => {
      const filter = { text: name, value: name };
      result[0].filters.push(filter);
    });
    Object.values(dates).forEach((date) => {
      const filter = { text: date, value: date };
      result[1].filters.push(filter);
    });

    return result;
  };

  return (
    <Modal
      title="Total asigned hours"
      visible={modalHours}
      onCancel={onCancel}
      footer={[]}
    >
      <Table
        columns={getColumnsTable(eventsList)}
        dataSource={getDataTable(eventsList)}
      />
    </Modal>
  );
};

export default HoursHandler;
