import React, { useContext } from "react";
import { Modal, Button } from "antd";
import { deleteEvent } from "../../helpers/firebase";
import axios from "axios";
import { InformationContext } from "../../context/InformationContext";
import { ViewContext } from "../../context/ViewContext";

export default function DeleteActivityModal() {
  let { onCancel, modalDelete: visible } = useContext(ViewContext);
  const { selectedEvent, setSelectedEvent, path, idEvent } =
    useContext(InformationContext);

  const onClick = () => {
    const idE = selectedEvent.idEvent;
    const idA = idEvent && selectedEvent.idActivity;
    const urlA = `https://api.evius.co/api/events/${idEvent}/activities/${idA}`;
    idE && axios.delete(`https://api.evius.co/api/events/${idE}`);
    idA && axios.delete(urlA);
    deleteEvent(selectedEvent, path);
    setSelectedEvent({});
    onCancel();
  };

  const modalProps = { title: "Delete", visible, onCancel };
  return (
    <Modal
      {...modalProps}
      footer={[<Button {...{ key: "Crear", onClick }}>Delete</Button>]}
    >
      <h2>Are you sure to delete this?</h2>
    </Modal>
  );
}
