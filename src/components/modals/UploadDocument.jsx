import React, { useContext, useState } from "react";
import { Button, message, Modal, Upload } from "antd";
import { ViewContext } from "../../context/ViewContext";
import { InformationContext } from "../../context/InformationContext";
import { UploadOutlined } from "@ant-design/icons";
import axios from "axios";

export const UploadDocument = () => {
  const { onCancel, modalUpload } = useContext(ViewContext);
  let { idEvent } = useContext(InformationContext);

  const [fileList, setFileList] = useState([]);
  const [uploading, setUploading] = useState(false);

  const onClick = () => {
    setUploading(true);
    const url = `https://api.evius.co/api/events/${idEvent}/documents`;
    axios
      .post(url, fileList[0])
      .then((res) => {
        console.log(res);
      })
      .catch(() => {
        message.error("Upload failed");
      })
      .then(() => {
        message.info("file uploaded successfully")
        setUploading(false);
      });
  };

  const props = {
    action: "https://api.evius.co/api/files/upload/",
    multiple: false,
    listType: "text",
    accept: "application/pdf",
    onChange: async ({ file }) => {
      const url = "https://api.evius.co/api/files/upload/";
      const formData = new FormData();
      console.log(file);
      formData.append("file", file.originFileObj);
      axios.post(url, formData).then((res) => {
        setFileList((e) => {
          const newFile = {
            name: file.name,
            title: file.name,
            format: "pdf",
            type: "file",
            file: res.data,
          };
          return [newFile];
        });
      });
    },
    beforeUpload: (file) => {
      const isPdf = file.type === "application/pdf";
      if (!isPdf) {
        message.error("You can only upload PDF file!");
      }
      const isLt5M = file.size / 1024 / 1024 < 5;
      if (!isLt5M) {
        message.error("Image must smaller than 5MB!");
      }
      const validation = isPdf && isLt5M;
      return validation;
    },
    fileList,
  };

  return (
    <Modal
      {...{ onCancel, visible: modalUpload, title: "Upload documents" }}
      footer={[
        <Button
          key={0}
          type="primary"
          onClick={onClick}
          disabled={fileList.length === 0}
          loading={uploading}
        >
          {uploading ? "Uploading" : "Start Upload"}
        </Button>,
      ]}
    >
      <Upload {...props}>
        <Button icon={<UploadOutlined />}>Select File</Button>
      </Upload>
    </Modal>
  );
};
