import React, { useContext } from "react";
import { Modal } from "antd";
import { InformationContext } from "../../context/InformationContext";
import { ViewContext } from "../../context/ViewContext";
import moment from "moment";

export default function SimpleDetailModal() {
  const { selectedEvent, setSelectedEvent } = useContext(InformationContext);
  const { onCancel, modalSimple } = useContext(ViewContext);
  
  if (!selectedEvent) return null;

  const handleCancel = () => {
    setSelectedEvent({});
    onCancel();
  };

  const start = moment(selectedEvent.start).format("YYYY-MM-DD HH:mm");
  const end = moment(selectedEvent.end).format("YYYY-MM-DD HH:mm");
  const name = selectedEvent.name;
  const desc = selectedEvent.description;
  const empl = selectedEvent.employee;
  const asig = selectedEvent.asignation;
  const type = selectedEvent.type;

  return (
    <Modal
      title="Information"
      visible={modalSimple}
      onCancel={handleCancel}
      footer={[]}
    >
      <b>Name: </b> <span>{name}</span> <br />
      <b>Description: </b> <span>{desc}</span> <br />
      <b>Asignee: </b> <span>{empl}</span> <br />
      <b>Space: </b> <span>{asig}</span> <br />
      <b>Type: </b> <span>{type}</span> <br />
      <b>Start: </b> <span>{start}</span> <br />
      <b>end: </b> <span>{end}</span> <br />
    </Modal>
  );
}
