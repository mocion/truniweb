import React from "react";
import { Menu, Dropdown, message } from "antd";
import { DownOutlined } from "@ant-design/icons";

export default function DropdownInfo({ setValue, value, data }) {
  const onClick = ({ domEvent }) => {
    setValue(domEvent.target.textContent);
    message.success("Value changed");
  };

  const menu = (
    <Menu onClick={onClick}>
      {data.map((item) => {
        const id = item?.id
        return id ? <Menu.Item key={id}>{item.name}</Menu.Item> : null;
      })}
    </Menu>
  );

  return (
    <>
      <Dropdown overlay={menu} trigger={["click"]}>
        <a href="*" className="ant-dropdown-link" onClick={(e) => e.preventDefault()}>
          {value} <DownOutlined />
        </a>
      </Dropdown>
    </>
  );
}
