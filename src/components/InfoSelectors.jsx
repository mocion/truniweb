import { Input, Space, Button } from "antd";
import React, { useContext } from "react";
import { InformationContext } from "../context/InformationContext";
import DropdownInfo from "./dropdownInfo.jsx";
import TimeDatePicker from "./timeDatePicker";
import EviusReactQuill from "./../components/EviusReactQuill";

const { TextArea } = Input;

export default function InfoSelectors({
  showActivities,
  activities,
  url,
  listCounter,
}) {
  let { type, users, asignations, selectedEvent, setSelectedEvent } =
    useContext(InformationContext);

  const setSelectedEventType = (selectedType) => {
    setSelectedEvent((event) => {
      const newEvent = { ...event, type: selectedType };
      return newEvent;
    });
  };

  const setSelectedEventEmployee = (employee) => {
    const user = users.find((a) => a.name === employee);
    setSelectedEvent((event) => {
      const copy = { ...event };
      if (user && user.id && user.name) {
        copy.idEmployee = user.id;
        copy.employee = user.name;
        copy.email = user.email;
      }
      return copy;
    });
  };

  const setSelectedEventSpace = (space) => {
    const asig = asignations.find((a) => a.name === space);
    setSelectedEvent((event) => {
      const copy = { ...event };
      if (asig && asig.id && asig.name) {
        copy.idAsignation = asig.id;
        copy.asignation = asig.name;
      }
      return copy;
    });
  };

  const handleInput = (e) => {
    setSelectedEvent((event) => {
      const newValue = { ...event, name: e.target.value };
      return newValue;
    });
  };

  const handleInputQuill = (content) => {
    setSelectedEvent((event) => {
      const newValue = { ...event, description: content };
      return newValue;
    });
  };

  return (
    <>
      <b>Name: </b>
      <TextArea
        defaultValue={selectedEvent.name || ""}
        value={selectedEvent.name}
        onChange={handleInput}
        rows={1}
      />
      {selectedEvent.idEvent && (
        <div style={{ padding: "10px 0" }}>
          <Button type="primary" key="Save" onClick={showActivities}>
            This event has a schedule
            {activities && " with " + listCounter + " activities"} Show
            activities
          </Button>
        </div>
      )}
      <b>Date: </b> <TimeDatePicker /> <br />
      <b>Assignee: </b>
      <DropdownInfo
        setValue={setSelectedEventEmployee}
        value={selectedEvent.employee}
        data={users}
      />
      <br />
      <b>Space: </b>
      <DropdownInfo
        setValue={setSelectedEventSpace}
        value={selectedEvent.asignation}
        data={asignations}
      />
      <br />
      <b>Type: </b>
      <DropdownInfo
        setValue={setSelectedEventType}
        value={selectedEvent.type}
        data={type}
      />
      <br />
      <b>Description: </b> <br />
      <EviusReactQuill
        id="description"
        name={"description"}
        data={selectedEvent.description ? selectedEvent.description : ""}
        handleChange={handleInputQuill}
        style={{ marginTop: "5px", minHeight: "300px" }}
      />
      {selectedEvent.idEvent && (
        <div>
          <Space direction="vertical">
            <a href={url}>Event advance settings</a>
          </Space>
        </div>
      )}
      <br />
    </>
  );
}
