import React, { useContext, useState } from "react";
import { Input, Button, message, Space, Card } from "antd";
import { sendToFilteredTokens } from "../helpers/firebase";
import DropdownInfo from "./dropdownInfo";
import { InformationContext } from "../context/InformationContext";

const Notifications = () => {
  const defaultFilterValue = "all staff?";
  const { users } = useContext(InformationContext);
  const [title, setTitle] = useState("");
  const [messageContent, setMessage] = useState("");
  const [filter, setFilter] = useState(defaultFilterValue);

  const list = [
    { id: 0, name: defaultFilterValue },
    { id: 1, name: "user" },
    { id: 2, name: "admin" },
  ];

  const handleClick = () => {
    if (title === "" || messageContent === "") {
      message.error("You must asign a title and a message");
    } else {
      try {
        if (filter === defaultFilterValue) {
          sendToFilteredTokens(
            users,
            "rol",
            filter,
            title,
            messageContent,
            false
          );
        } else {
          sendToFilteredTokens(
            users,
            "rol",
            filter,
            title,
            messageContent,
            true
          );
        }
        setTitle("");
        setMessage("");
        setFilter(defaultFilterValue);
        message.info("Notifications sent");
      } catch (error) {
        message.error("We have a problem whit this proccess, try again later");
      }
    }
  };

  return (
    <Card>
      <Space direction="vertical" style={{ width: "100%" }}>
        <h3>{"New notification to:"}</h3>
        <DropdownInfo setValue={setFilter} value={filter} data={list} />
        <h3>{"Title: "}</h3>
        <Input
          onChange={(e) => setTitle(e.target.value)}
          value={title}
          style={{ width: "70%", height: 50 }}
        />
        <h3>{"Message: "}</h3>
        <Input
          onChange={(e) => setMessage(e.target.value)}
          value={messageContent}
          style={{ width: "70%", height: 100 }}
        />
      </Space>
      <Button
        type="primary"
        htmlType="submit"
        onClick={handleClick}
        style={{ margin: 20 }}
      >
        {`Send to: ${filter}`}
      </Button>
    </Card>
  );
};

export default Notifications;
