import React, { useContext, useRef } from "react";
import { Space, Button, PageHeader } from "antd";
import {
  FilterOutlined,
  LogoutOutlined,
  UploadOutlined,
} from "@ant-design/icons";
import { logoutUser } from "../helpers/firebase.jsx";
import "antd/dist/antd.css";
import { ViewContext } from "../context/ViewContext.jsx";
import { InformationContext } from "../context/InformationContext.jsx";
import { EVIUS_URL } from "../helpers/configurations";
import { useNavigate } from "react-router-dom";
import { UserContext } from "../context/UserContext";
import { Calendar } from "../screens/Calendar";
import ReactToPrint from "react-to-print";

export const ComponentToPrint = React.forwardRef((props, ref) => {
  return (
    <div ref={ref}>
      <Calendar />
    </div>
  );
});

export const HeaderContent = () => {
  const {
    handleModalFilter,
    handleModalUpload,
    viewCms,
    viewUser,
    viewSpace,
    handleViewCms,
    handleViewUser,
    handleViewSpace,
  } = useContext(ViewContext);
  let { setIdEvent, idEvent, nameEvent, setUserLogin } =
    useContext(InformationContext);
  const { user, userIsLoading } = useContext(UserContext);
  const componentRef = useRef();

  const navigate = useNavigate();

  const navigateCMS = () => {
    handleViewCms();
    navigate("/cms");
  };

  const navigateUser = () => {
    handleViewUser();
    navigate("/");
  };

  const navigateSpace = () => {
    handleViewSpace();
    navigate("/");
  };

  return (
    <PageHeader
      className="site-page-header"
      onBack={idEvent ? () => setIdEvent(null) : false}
      title={
        idEvent ? (
          <a
            target="_blank"
            rel="noreferrer"
            id="event_title_name"
            href={EVIUS_URL + "/landing/" + idEvent}
          >
            <span
              style={{ display: "inline" }}
              dangerouslySetInnerHTML={{
                __html:
                  nameEvent +
                  " <span style='display:inline'>View Landing Page</span>",
              }}
            />
          </a>
        ) : (
          "Events"
        )
      }
      extra={[
        <Space key={0}>
          <ReactToPrint
            trigger={() => <button>Print</button>}
            content={() => componentRef.current}
          />
          <b>Options:</b>
          <Button
            disabled={viewCms}
            onClick={handleModalFilter}
            shape="circle"
            icon={<FilterOutlined />}
          />
          {idEvent && (
            <Button
              disabled={viewCms}
              onClick={handleModalUpload}
              shape="circle"
              icon={<UploadOutlined />}
            />
          )}
          {false && <b>View:</b>}
          {false && (
            <Button
              type={viewUser ? "primary" : "default"}
              onClick={navigateUser}
            >
              Employee
            </Button>
          )}
          {false && (
            <Button
              type={viewSpace ? "primary" : "default"}
              onClick={navigateSpace}
            >
              Asignation
            </Button>
          )}
          {false && (
            <Button
              type={viewCms ? "primary" : "default"}
              onClick={navigateCMS}
            >
              CMS
            </Button>
          )}
          {!userIsLoading &&
            (user ? (
              <Button
                onClick={() => logoutUser(setUserLogin)}
                shape="circle"
                icon={<LogoutOutlined />}
              >
                <span>{user.email.charAt().toUpperCase() || ""} </span>
              </Button>
            ) : (
              <Button onClick={() => navigate("/login")}>Login</Button>
            ))}
          <div style={{ display: "none" }}>
            <ComponentToPrint ref={componentRef} />
          </div>
        </Space>,
      ]}
    />
  );
};
