import React, { useState } from "react";
import { Form, Input, Button, Card, message, Space, Modal } from "antd";
import DropdownInfo from "./dropdownInfo";
import { db } from "../helpers/firebase";

const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};

export const CmsItem = ({ setter, title, list }) => {
  // title is related whit the doc name in firebase
  // this is important don´t change
  const defaultValue = `Select ${title}`;
  const [dropdownValue, setDropdownValue] = useState(defaultValue);
  const [inputValue, setInputValue] = useState("");
  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    const selected = list.find((a) => a.names === dropdownValue);
    db.collection(`deleted${title}`).doc(`${selected.id}`).set(selected);
    db.collection(title).doc(`${selected.id}`).delete();
    message.success(`${title} was deleted`);
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const handleChange = (e) => {
    setInputValue(e.target.value);
  };

  const handleClick = () => {
    if (inputValue !== "") {
      const newId =
        Date.now() +
        Math.floor(Math.random() * Math.floor(Math.random() * Date.now()));

      let color = "#";
      for (let i = 0; i < 3; i++)
        color += (
          "0" +
          Math.floor(((1 + Math.random()) * Math.pow(16, 2)) / 2).toString(16)
        ).slice(-2);

      var data = {
        id: newId,
        name: inputValue,
        hexColor: color,
      };

      if (dropdownValue !== defaultValue) {
        const selected = list.find((a) => a.names === dropdownValue);

        data.id = selected.id;
        data.hexColor = selected.hexColor;
        handleReset();
        message.success(`Modification made successfully`);
      } else {
        message.success(`New ${title} created`);
      }
      setter(title, data);
    } else {
      message.error("Please enter a name");
    }
  };

  const handleDelete = () => {
    if (dropdownValue === defaultValue) {
      message.error("Please select someone to delete");
    } else {
      const selected = list.find((a) => a.names === dropdownValue);
      if (selected.permanent) {
        handleReset();
        message.error(`This is a permanent ${title}, you can't delete it`);
      } else {
        showModal();
      }
    }
  };

  const handleReset = () => {
    setDropdownValue(defaultValue);
    setInputValue("");
  };

  return (
    <Card>
      <h3>{`Modify someone?`}</h3>
      <DropdownInfo
        setValue={setDropdownValue}
        value={dropdownValue}
        data={list}
      />
      <h3>
        {dropdownValue === defaultValue
          ? `Create new ${title}`
          : `Modify ${dropdownValue}`}
      </h3>
      <Form {...layout} name="nest-messages">
        <Form.Item
          label="Name"
          rules={[
            {
              required: true,
            },
          ]}
        >
          <Input onChange={handleChange} value={inputValue} />
        </Form.Item>
        <Space>
          <Button onClick={handleDelete}>Delete</Button>
          <Button onClick={handleReset}>Reset</Button>
          <Button type="primary" htmlType="submit" onClick={handleClick}>
            {dropdownValue === defaultValue ? "Create" : "Modify"}
          </Button>
        </Space>
      </Form>
      <Modal
        title="Caution!"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <p>Before you erase this, you need to know:</p>
        <p>You have to delete all the relationated</p>
        <p>assignations, are you sure to continue?</p>
      </Modal>
    </Card>
  );
};
