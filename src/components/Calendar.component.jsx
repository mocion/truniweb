import React from "react";
import { Calendar as Cal, momentLocalizer } from "react-big-calendar";
import withDragAndDrop from "react-big-calendar/lib/addons/dragAndDrop";
import "react-big-calendar/lib/css/react-big-calendar.css";
import "react-big-calendar/lib/addons/dragAndDrop/styles.css";

const DragAndDropCalendar = withDragAndDrop(Cal);
let moment = require("moment");
moment = require("moment-timezone");

export const CalendarComponent = ({
  events = [],
  date = new Date(),
  onNavigate,
  view = "day",
  onView,
  views = ["day", "week", "month", "agenda"],
  getNow = () => new Date(),
  accessors,
  selectable = false,
  ...props
}) => {
  const localizer = momentLocalizer(moment);
  const today = new Date();
  const datestart = new Date(
    today.getFullYear(), 
    today.getMonth(), 
    today.getDate(), 
    6
  )
  return (
    <>
      <DragAndDropCalendar
        {...{
          localizer,
          events,
          date,
          onNavigate,
          view,
          onView,
          views,
          getNow,
          accessors,
          selectable,
          min:datestart
        }}
        resizable
        {...props}
      />
    </>
  );
};
