import React from "react";
import { GoogleMap, withScriptjs, withGoogleMap } from "react-google-maps";

const MapView = (props) => {
  return (
    <GoogleMap
      defaultZoom={15}
      defaultCenter={{ lat: 4.672016374453099, lng: -74.08381909957575 }}
    />
  );
};

export default withScriptjs(withGoogleMap(MapView));

{/* <Map
  googleMapURL={`https://maps.googleapis.com/maps/api/js?v+3.exp&key=${credentials.mapsKey}`}
  containerElement={<div style={{ height: "400px" }} />}
  mapElement={<div style={{ height: "100%" }} />}
  loadingElement={<p>Loading...</p>}
/> */}
