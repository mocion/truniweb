import React, { useContext } from "react";
import { RRule } from "rrule";
import { DatePicker, Checkbox } from "antd";
import { InformationContext } from "../context/InformationContext";
const { RangePicker } = DatePicker;

export const RecurrentActivities = () => {
  let { idEvent } = useContext(InformationContext);

  const [checkedValues, setCheckedValues] = useState([]);
  const [date, setDate] = useState();
  
  const onChange = (e) => setNewActivity(e.target.checked);
  
  const createByRange = async () => {
    const rangeOfDates = RecurrentActivities({
      date,
      checkedValues,
      selectedEvent,
    });

    rangeOfDates.forEach(async (object) => {
      selectedEvent.start = object.start;
      selectedEvent.end = object.end;
      selectedEvent.id =
        Date.now() +
        Math.floor(Math.random() * Math.floor(Math.random() * Date.now()));
      await setNewDoc(path, {
        ...selectedEvent,
      });
    });
  };
  
  const byDefault =
    "By default this will create one shift for everyday using the hours selected above, (x) clean recurrent dates to cancel this action";
  return (
    <>
      <b>
        This option allows you to create a new
        {idEvent ? " activity:" : " event:"}
      </b>
      <br />
      <Checkbox checked={isNewActivity} onChange={onChange}>
        Create new {idEvent ? "activity" : "event"}
      </Checkbox>
      <br />

      <b>Recurrent activity: </b> <br />
      <RangePicker onChange={(range) => setDate(range)} /> <br />
      {byDefault}
      {date && (
        <Checkbox.Group
          options={checkboxOptions}
          onChange={(checkedValues) => setCheckedValues(checkedValues)}
        />
      )}
    </>
  );
};

const checkboxOptions = [
  { label: "Mo", value: RRule.MO },
  { label: "Tu", value: RRule.TU },
  { label: "We", value: RRule.WE },
  { label: "Th", value: RRule.TH },
  { label: "Fr", value: RRule.FR },
  { label: "Sa", value: RRule.SA },
  { label: "Su", value: RRule.SU },
];
