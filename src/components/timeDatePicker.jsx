import React, { useState, useEffect, useContext } from "react";
import { DatePicker } from "antd";
import moment from "moment";
import { InformationContext } from "../context/InformationContext";
const { RangePicker } = DatePicker;

export default function TimeDatePicker() {
  let { selectedEvent, setSelectedEvent } = useContext(InformationContext);
  const [event, setEvent] = useState(false);
  const range = [moment(selectedEvent.start), moment(selectedEvent.end)];
  const time = { format: "HH:mm" };
  const date = "YY-MM-DD HH:mm";

  useEffect(() => {
    setEvent(false);
  }, [event]);

  const changeDay = (value) => {
    value &&
    setSelectedEvent((event) => {
        const newValue = { ...event };
        newValue.start = value[0]._d;
        newValue.end = value[1]._d;
        return newValue;
      });
  };

  return (
    <RangePicker
      value={range}
      showTime={time}
      format={date}
      minuteStep={15}
      onOk={changeDay}
    />
  );
}
