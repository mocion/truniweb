import React from "react";
import "./App.css";
import "antd/dist/antd.css";
import { CalendarContextProvider } from "./context/CalendarContext";
import { InformationContextProvider } from "./context/InformationContext";
import { ViewContextProvider } from "./context/ViewContext";
import { UserContextProvider } from "./context/UserContext";
import { MainRoutes } from "./router/MainRoutes";

function App() {
  return (
    <div className="App">
      <UserContextProvider>
      <InformationContextProvider>
        <ViewContextProvider>
          <CalendarContextProvider>
            <MainRoutes />
          </CalendarContextProvider>
        </ViewContextProvider>
      </InformationContextProvider>
      </UserContextProvider>
    </div>
  );
}

export default App;
